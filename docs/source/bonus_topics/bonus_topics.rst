Bonus topics
========================

.. toctree::
   :maxdepth: 1

   gpaw
   analyzing_databases
   parallelization.rst
