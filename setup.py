import re
from setuptools import setup, find_packages
from setuptools.extension import Extension
from Cython.Build import cythonize, build_ext
import numpy

extensions = [
    Extension(
        "agox.models.GPR.priors.repulsive", ["agox/models/GPR/priors/repulsive.pyx"], include_dirs=[numpy.get_include()]
    ),
    Extension(
        "agox.models.descriptors.fingerprint_cython.angular_fingerprintFeature_cy",
        ["agox/models/descriptors/fingerprint_cython/angular_fingerprintFeature_cy.pyx"],
        include_dirs=[numpy.get_include()],
    ),
]

# Version Number:
version_file = "agox/__init__.py"
with open(version_file) as f:
    lines = f.readlines()

for line in lines:
    if "__version_info__" in line:
        result = re.findall("\d+", line)
        result = [int(x) for x in result]
        version = "{}.{}.{}".format(*result)
        break


minimum_requirements = [
    "numpy",
    "ase",
    "matplotlib",
    "cymem",
    "scikit-learn",
    "h5py",
    "matscipy",
    "importlib-resources > 1.3 ; python_version < '3.9'",
]
full_requirements = [
    "dscribe>=2.0.0",
    "ray[default]<=2.12.0",
    "pytest",
]
extras = [
    "schnetpack",
    "tensorboard",
]

docs = ['sphinx', 
        'furo',
        'sphinx-autoapi',
        'sphinx-copybutton',
        'sphinx-tabs',
        ]


setup(
    name="agox",
    version=version,
    url="https://agox.gitlab.io/agox/",
    description="Atomistic Global Optimziation X is a framework for structure optimization in materials science.",
    install_requires=minimum_requirements,
    extras_require={
        "full": full_requirements,
        "extras": extras,
        "docs": docs,
    },
    python_requires=">= 3.8, < 3.12",
    packages=find_packages(),
    include_package_data=True,
    ext_modules=cythonize(extensions),
    entry_points={
        "console_scripts": [
            "agox=agox.cli.main:main",
            "agox-convert=agox.utils.convert_database:convert",
            "agox-analysis=agox.utils.batch_analysis:command_line_analysis",
            'agox-graph-analysis=agox.utils.graph_sorting:analysis',
            ]
        },
)