from .GPR import GPR
from .sGPR import SparseGPR

__all__ = ['GPR', 'SparseGPR']