__version_info__ = (3, 6, 0)
__version__ = '{}.{}.{}'.format(*__version_info__)

from agox.module import Module
from agox.observer import Observer
from agox.writer import Writer
from agox.tracker import Tracker
from agox.main import AGOX

__all__ = ['Module', 'Observer', 'Writer', 'Tracker', 'AGOX', '__version__']


